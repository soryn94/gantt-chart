export default [
  {
    id: 41938,
    departureDate: '2020-09-01T10:33:00.000Z',
    arrivalDate: '2020-09-01T08:58:00.000Z',
    materialNumber: '4135',
    trainNumberArrival: '3593',
    trainNumberDeparture: '2515',
    trainPosition: '101',
    trainStation: null,
    trainIOD: true,
    noDepartureTime: false,
    hidden: false,
    locationId: 5341,
    serviceWindowId: 41969,
    location: {
      id: 5341,
      locationNumber: 654000,
      name: 'Utrecht',
      region: 'Regio Randstad Zuid',
      locationType: 'SB',
      code: null,
      shuntingTime: null,
      minOverstandMinutes: null,
      station: 'UT',
      NTTstation: null,
      isWorkLocation: null,
      implemented: false,
      autoplan: false,
      autoplanComodity: false,
      address: null,
      commodityGroups: null,
      distance: null,
      defaultAssignmentPersonId: null,
      defaultReasonMaintenance: 'Service',
      knoopPunt: null,
    },
    serviceWindow: {
      id: 41969,
      serviceWindowStart: '2020-09-01T08:58:00.000Z',
      serviceWindowEnd: '2020-09-01T10:33:00.000Z',
      materialNumber: null,
      emplacement: [
        {
          id: 28467,
          emplacementStart: '2020-09-01T08:58:00.000Z',
          emplacementEnd: '2020-09-01T10:33:00.000Z',
          durationInMinutes: 95,
          maintenanceTrack: null,
          inConflict: false,
          serviceWindowId: 41969,
        },
      ],
    },
  },
  {
    id: 42006,
    departureDate: '2020-09-01T14:11:00.000Z',
    arrivalDate: '2020-09-01T10:35:00.000Z',
    materialNumber: '16438',
    trainNumberArrival: '2879',
    trainNumberDeparture: '3578',
    trainPosition: '101',
    trainStation: null,
    trainIOD: true,
    noDepartureTime: false,
    hidden: false,
    locationId: 5341,
    serviceWindowId: 42033,
    location: {
      id: 5341,
      locationNumber: 654000,
      name: 'Utrecht',
      region: 'Regio Randstad Zuid',
      locationType: 'SB',
      code: null,
      shuntingTime: null,
      minOverstandMinutes: null,
      station: 'UT',
      NTTstation: null,
      isWorkLocation: null,
      implemented: false,
      autoplan: false,
      autoplanComodity: false,
      address: null,
      commodityGroups: null,
      distance: null,
      defaultAssignmentPersonId: null,
      defaultReasonMaintenance: 'Service',
      knoopPunt: null,
    },
    serviceWindow: {
      id: 42033,
      serviceWindowStart: '2020-09-01T10:35:00.000Z',
      serviceWindowEnd: '2020-09-01T14:11:00.000Z',
      materialNumber: null,
      emplacement: [
        {
          id: 28531,
          emplacementStart: '2020-09-01T10:35:00.000Z',
          emplacementEnd: '2020-09-01T14:11:00.000Z',
          durationInMinutes: 216,
          maintenanceTrack: null,
          inConflict: false,
          serviceWindowId: 42033,
        },
      ],
    },
  },
  {
    id: 41966,
    departureDate: '2020-09-01T14:11:00.000Z',
    arrivalDate: '2020-09-01T10:35:00.000Z',
    materialNumber: '2989',
    trainNumberArrival: '2879',
    trainNumberDeparture: '3578',
    trainPosition: '101',
    trainStation: null,
    trainIOD: true,
    noDepartureTime: false,
    hidden: false,
    locationId: 5341,
    serviceWindowId: 41993,
    location: {
      id: 5341,
      locationNumber: 654000,
      name: 'Utrecht',
      region: 'Regio Randstad Zuid',
      locationType: 'SB',
      code: null,
      shuntingTime: null,
      minOverstandMinutes: null,
      station: 'UT',
      NTTstation: null,
      isWorkLocation: null,
      implemented: false,
      autoplan: false,
      autoplanComodity: false,
      address: null,
      commodityGroups: null,
      distance: null,
      defaultAssignmentPersonId: null,
      defaultReasonMaintenance: 'Service',
      knoopPunt: null,
    },
    serviceWindow: {
      id: 41993,
      serviceWindowStart: '2020-09-01T10:35:00.000Z',
      serviceWindowEnd: '2020-09-01T14:11:00.000Z',
      materialNumber: null,
      emplacement: [
        {
          id: 28491,
          emplacementStart: '2020-09-01T10:35:00.000Z',
          emplacementEnd: '2020-09-01T14:11:00.000Z',
          durationInMinutes: 216,
          maintenanceTrack: null,
          inConflict: false,
          serviceWindowId: 41993,
        },
      ],
    },
  },
  {
    id: 41986,
    departureDate: '2020-09-01T14:11:00.000Z',
    arrivalDate: '2020-09-01T10:35:00.000Z',
    materialNumber: '4047',
    trainNumberArrival: '2879',
    trainNumberDeparture: '3578',
    trainPosition: '101',
    trainStation: null,
    trainIOD: true,
    noDepartureTime: false,
    hidden: false,
    locationId: 5341,
    serviceWindowId: 42013,
    location: {
      id: 5341,
      locationNumber: 654000,
      name: 'Utrecht',
      region: 'Regio Randstad Zuid',
      locationType: 'SB',
      code: null,
      shuntingTime: null,
      minOverstandMinutes: null,
      station: 'UT',
      NTTstation: null,
      isWorkLocation: null,
      implemented: false,
      autoplan: false,
      autoplanComodity: false,
      address: null,
      commodityGroups: null,
      distance: null,
      defaultAssignmentPersonId: null,
      defaultReasonMaintenance: 'Service',
      knoopPunt: null,
    },
    serviceWindow: {
      id: 42013,
      serviceWindowStart: '2020-09-01T10:35:00.000Z',
      serviceWindowEnd: '2020-09-01T14:11:00.000Z',
      materialNumber: null,
      emplacement: [
        {
          id: 28511,
          emplacementStart: '2020-09-01T10:35:00.000Z',
          emplacementEnd: '2020-09-01T14:11:00.000Z',
          durationInMinutes: 216,
          maintenanceTrack: null,
          inConflict: false,
          serviceWindowId: 42013,
        },
      ],
    },
  },
  {
    id: 41934,
    departureDate: '2020-09-01T17:33:00.000Z',
    arrivalDate: '2020-09-01T13:59:00.000Z',
    materialNumber: '4135',
    trainNumberArrival: '3593',
    trainNumberDeparture: '2515',
    trainPosition: '101',
    trainStation: null,
    trainIOD: true,
    noDepartureTime: false,
    hidden: false,
    locationId: 5341,
    serviceWindowId: 41965,
    location: {
      id: 5341,
      locationNumber: 654000,
      name: 'Utrecht',
      region: 'Regio Randstad Zuid',
      locationType: 'SB',
      code: null,
      shuntingTime: null,
      minOverstandMinutes: null,
      station: 'UT',
      NTTstation: null,
      isWorkLocation: null,
      implemented: false,
      autoplan: false,
      autoplanComodity: false,
      address: null,
      commodityGroups: null,
      distance: null,
      defaultAssignmentPersonId: null,
      defaultReasonMaintenance: 'Service',
      knoopPunt: null,
    },
    serviceWindow: {
      id: 41965,
      serviceWindowStart: '2020-09-01T13:59:00.000Z',
      serviceWindowEnd: '2020-09-01T17:33:00.000Z',
      materialNumber: null,
      emplacement: [
        {
          id: 28463,
          emplacementStart: '2020-09-01T13:59:00.000Z',
          emplacementEnd: '2020-09-01T17:33:00.000Z',
          durationInMinutes: 214,
          maintenanceTrack: null,
          inConflict: false,
          serviceWindowId: 41965,
        },
      ],
    },
  },
  {
    id: 41906,
    departureDate: '2020-09-01T18:11:00.000Z',
    arrivalDate: '2020-09-01T15:35:00.000Z',
    materialNumber: '7536',
    trainNumberArrival: '2879',
    trainNumberDeparture: '3578',
    trainPosition: '101',
    trainStation: null,
    trainIOD: true,
    noDepartureTime: false,
    hidden: false,
    locationId: 5341,
    serviceWindowId: 41937,
    location: {
      id: 5341,
      locationNumber: 654000,
      name: 'Utrecht',
      region: 'Regio Randstad Zuid',
      locationType: 'SB',
      code: null,
      shuntingTime: null,
      minOverstandMinutes: null,
      station: 'UT',
      NTTstation: null,
      isWorkLocation: null,
      implemented: false,
      autoplan: false,
      autoplanComodity: false,
      address: null,
      commodityGroups: null,
      distance: null,
      defaultAssignmentPersonId: null,
      defaultReasonMaintenance: 'Service',
      knoopPunt: null,
    },
    serviceWindow: {
      id: 41937,
      serviceWindowStart: '2020-09-01T15:35:00.000Z',
      serviceWindowEnd: '2020-09-01T18:11:00.000Z',
      materialNumber: null,
      emplacement: [
        {
          id: 28435,
          emplacementStart: '2020-09-01T15:35:00.000Z',
          emplacementEnd: '2020-09-01T18:11:00.000Z',
          durationInMinutes: 156,
          maintenanceTrack: null,
          inConflict: false,
          serviceWindowId: 41937,
        },
      ],
    },
  },
  {
    id: 41910,
    departureDate: '2020-09-01T21:00:00.000Z',
    arrivalDate: '2020-09-01T14:56:00.000Z',
    materialNumber: '2958',
    trainNumberArrival: '7471',
    trainNumberDeparture: '7415',
    trainPosition: '101',
    trainStation: null,
    trainIOD: true,
    noDepartureTime: false,
    hidden: false,
    locationId: 5341,
    serviceWindowId: 41941,
    location: {
      id: 5341,
      locationNumber: 654000,
      name: 'Utrecht',
      region: 'Regio Randstad Zuid',
      locationType: 'SB',
      code: null,
      shuntingTime: null,
      minOverstandMinutes: null,
      station: 'UT',
      NTTstation: null,
      isWorkLocation: null,
      implemented: false,
      autoplan: false,
      autoplanComodity: false,
      address: null,
      commodityGroups: null,
      distance: null,
      defaultAssignmentPersonId: null,
      defaultReasonMaintenance: 'Service',
      knoopPunt: null,
    },
    serviceWindow: {
      id: 41941,
      serviceWindowStart: '2020-09-01T14:56:00.000Z',
      serviceWindowEnd: '2020-09-01T21:00:00.000Z',
      materialNumber: null,
      emplacement: [
        {
          id: 28439,
          emplacementStart: '2020-09-01T14:56:00.000Z',
          emplacementEnd: '2020-09-01T21:00:00.000Z',
          durationInMinutes: 364,
          maintenanceTrack: null,
          inConflict: false,
          serviceWindowId: 41941,
        },
      ],
    },
  },
  {
    id: 41946,
    departureDate: '2020-09-01T21:33:00.000Z',
    arrivalDate: '2020-09-01T16:59:00.000Z',
    materialNumber: '4155',
    trainNumberArrival: '3597',
    trainNumberDeparture: '2517',
    trainPosition: '101',
    trainStation: null,
    trainIOD: true,
    noDepartureTime: false,
    hidden: false,
    locationId: 5341,
    serviceWindowId: 41977,
    location: {
      id: 5341,
      locationNumber: 654000,
      name: 'Utrecht',
      region: 'Regio Randstad Zuid',
      locationType: 'SB',
      code: null,
      shuntingTime: null,
      minOverstandMinutes: null,
      station: 'UT',
      NTTstation: null,
      isWorkLocation: null,
      implemented: false,
      autoplan: false,
      autoplanComodity: false,
      address: null,
      commodityGroups: null,
      distance: null,
      defaultAssignmentPersonId: null,
      defaultReasonMaintenance: 'Service',
      knoopPunt: null,
    },
    serviceWindow: {
      id: 41977,
      serviceWindowStart: '2020-09-01T16:59:00.000Z',
      serviceWindowEnd: '2020-09-01T21:33:00.000Z',
      materialNumber: null,
      emplacement: [
        {
          id: 28475,
          emplacementStart: '2020-09-01T16:59:00.000Z',
          emplacementEnd: '2020-09-01T21:33:00.000Z',
          durationInMinutes: 274,
          maintenanceTrack: null,
          inConflict: false,
          serviceWindowId: 41977,
        },
      ],
    },
  },
  {
    id: 41979,
    departureDate: '2020-09-02T05:11:00.000Z',
    arrivalDate: '2020-09-02T01:35:00.000Z',
    materialNumber: '2989',
    trainNumberArrival: '2879',
    trainNumberDeparture: '3578',
    trainPosition: '101',
    trainStation: null,
    trainIOD: true,
    noDepartureTime: false,
    hidden: false,
    locationId: 5341,
    serviceWindowId: 42006,
    location: {
      id: 5341,
      locationNumber: 654000,
      name: 'Utrecht',
      region: 'Regio Randstad Zuid',
      locationType: 'SB',
      code: null,
      shuntingTime: null,
      minOverstandMinutes: null,
      station: 'UT',
      NTTstation: null,
      isWorkLocation: null,
      implemented: false,
      autoplan: false,
      autoplanComodity: false,
      address: null,
      commodityGroups: null,
      distance: null,
      defaultAssignmentPersonId: null,
      defaultReasonMaintenance: 'Service',
      knoopPunt: null,
    },
    serviceWindow: {
      id: 42006,
      serviceWindowStart: '2020-09-02T01:35:00.000Z',
      serviceWindowEnd: '2020-09-02T05:11:00.000Z',
      materialNumber: null,
      emplacement: [
        {
          id: 28504,
          emplacementStart: '2020-09-02T01:35:00.000Z',
          emplacementEnd: '2020-09-02T05:11:00.000Z',
          durationInMinutes: 216,
          maintenanceTrack: null,
          inConflict: false,
          serviceWindowId: 42006,
        },
      ],
    },
  },
  {
    id: 41999,
    departureDate: '2020-09-02T05:11:00.000Z',
    arrivalDate: '2020-09-02T01:35:00.000Z',
    materialNumber: '4047',
    trainNumberArrival: '2879',
    trainNumberDeparture: '3578',
    trainPosition: '101',
    trainStation: null,
    trainIOD: true,
    noDepartureTime: false,
    hidden: false,
    locationId: 5341,
    serviceWindowId: 42026,
    location: {
      id: 5341,
      locationNumber: 654000,
      name: 'Utrecht',
      region: 'Regio Randstad Zuid',
      locationType: 'SB',
      code: null,
      shuntingTime: null,
      minOverstandMinutes: null,
      station: 'UT',
      NTTstation: null,
      isWorkLocation: null,
      implemented: false,
      autoplan: false,
      autoplanComodity: false,
      address: null,
      commodityGroups: null,
      distance: null,
      defaultAssignmentPersonId: null,
      defaultReasonMaintenance: 'Service',
      knoopPunt: null,
    },
    serviceWindow: {
      id: 42026,
      serviceWindowStart: '2020-09-02T01:35:00.000Z',
      serviceWindowEnd: '2020-09-02T05:11:00.000Z',
      materialNumber: null,
      emplacement: [
        {
          id: 28524,
          emplacementStart: '2020-09-02T01:35:00.000Z',
          emplacementEnd: '2020-09-02T05:11:00.000Z',
          durationInMinutes: 216,
          maintenanceTrack: null,
          inConflict: false,
          serviceWindowId: 42026,
        },
      ],
    },
  },
  {
    id: 42023,
    departureDate: '2020-09-02T05:11:00.000Z',
    arrivalDate: '2020-09-02T01:35:00.000Z',
    materialNumber: '16438',
    trainNumberArrival: '2879',
    trainNumberDeparture: '3578',
    trainPosition: '101',
    trainStation: null,
    trainIOD: true,
    noDepartureTime: false,
    hidden: false,
    locationId: 5341,
    serviceWindowId: 42050,
    location: {
      id: 5341,
      locationNumber: 654000,
      name: 'Utrecht',
      region: 'Regio Randstad Zuid',
      locationType: 'SB',
      code: null,
      shuntingTime: null,
      minOverstandMinutes: null,
      station: 'UT',
      NTTstation: null,
      isWorkLocation: null,
      implemented: false,
      autoplan: false,
      autoplanComodity: false,
      address: null,
      commodityGroups: null,
      distance: null,
      defaultAssignmentPersonId: null,
      defaultReasonMaintenance: 'Service',
      knoopPunt: null,
    },
    serviceWindow: {
      id: 42050,
      serviceWindowStart: '2020-09-02T01:35:00.000Z',
      serviceWindowEnd: '2020-09-02T05:11:00.000Z',
      materialNumber: null,
      emplacement: [
        {
          id: 28548,
          emplacementStart: '2020-09-02T01:35:00.000Z',
          emplacementEnd: '2020-09-02T05:11:00.000Z',
          durationInMinutes: 216,
          maintenanceTrack: null,
          inConflict: false,
          serviceWindowId: 42050,
        },
      ],
    },
  },
  {
    id: 41902,
    departureDate: '2020-09-02T06:00:00.000Z',
    arrivalDate: '2020-09-01T17:02:00.000Z',
    materialNumber: '2986',
    trainNumberArrival: '89032',
    trainNumberDeparture: '89032',
    trainPosition: '101',
    trainStation: null,
    trainIOD: true,
    noDepartureTime: false,
    hidden: false,
    locationId: 5341,
    serviceWindowId: 41933,
    location: {
      id: 5341,
      locationNumber: 654000,
      name: 'Utrecht',
      region: 'Regio Randstad Zuid',
      locationType: 'SB',
      code: null,
      shuntingTime: null,
      minOverstandMinutes: null,
      station: 'UT',
      NTTstation: null,
      isWorkLocation: null,
      implemented: false,
      autoplan: false,
      autoplanComodity: false,
      address: null,
      commodityGroups: null,
      distance: null,
      defaultAssignmentPersonId: null,
      defaultReasonMaintenance: 'Service',
      knoopPunt: null,
    },
    serviceWindow: {
      id: 41933,
      serviceWindowStart: '2020-09-01T17:02:00.000Z',
      serviceWindowEnd: '2020-09-02T06:00:00.000Z',
      materialNumber: null,
      emplacement: [
        {
          id: 28431,
          emplacementStart: '2020-09-01T17:02:00.000Z',
          emplacementEnd: '2020-09-02T06:00:00.000Z',
          durationInMinutes: 778,
          maintenanceTrack: null,
          inConflict: false,
          serviceWindowId: 41933,
        },
      ],
    },
  },
  {
    id: 41914,
    departureDate: '2020-09-02T06:06:00.000Z',
    arrivalDate: '2020-09-01T23:27:00.000Z',
    materialNumber: '2417',
    trainNumberArrival: '75589',
    trainNumberDeparture: '80021',
    trainPosition: '101',
    trainStation: null,
    trainIOD: true,
    noDepartureTime: false,
    hidden: false,
    locationId: 5341,
    serviceWindowId: 41945,
    location: {
      id: 5341,
      locationNumber: 654000,
      name: 'Utrecht',
      region: 'Regio Randstad Zuid',
      locationType: 'SB',
      code: null,
      shuntingTime: null,
      minOverstandMinutes: null,
      station: 'UT',
      NTTstation: null,
      isWorkLocation: null,
      implemented: false,
      autoplan: false,
      autoplanComodity: false,
      address: null,
      commodityGroups: null,
      distance: null,
      defaultAssignmentPersonId: null,
      defaultReasonMaintenance: 'Service',
      knoopPunt: null,
    },
    serviceWindow: {
      id: 41945,
      serviceWindowStart: '2020-09-01T23:27:00.000Z',
      serviceWindowEnd: '2020-09-02T06:06:00.000Z',
      materialNumber: null,
      emplacement: [
        {
          id: 28443,
          emplacementStart: '2020-09-01T23:27:00.000Z',
          emplacementEnd: '2020-09-02T06:06:00.000Z',
          durationInMinutes: 399,
          maintenanceTrack: null,
          inConflict: false,
          serviceWindowId: 41945,
        },
      ],
    },
  },
  {
    id: 41926,
    departureDate: '2020-09-02T06:33:00.000Z',
    arrivalDate: '2020-09-01T23:59:00.000Z',
    materialNumber: '7536',
    trainNumberArrival: '3587',
    trainNumberDeparture: '2814',
    trainPosition: '101',
    trainStation: null,
    trainIOD: true,
    noDepartureTime: false,
    hidden: false,
    locationId: 5341,
    serviceWindowId: 41957,
    location: {
      id: 5341,
      locationNumber: 654000,
      name: 'Utrecht',
      region: 'Regio Randstad Zuid',
      locationType: 'SB',
      code: null,
      shuntingTime: null,
      minOverstandMinutes: null,
      station: 'UT',
      NTTstation: null,
      isWorkLocation: null,
      implemented: false,
      autoplan: false,
      autoplanComodity: false,
      address: null,
      commodityGroups: null,
      distance: null,
      defaultAssignmentPersonId: null,
      defaultReasonMaintenance: 'Service',
      knoopPunt: null,
    },
    serviceWindow: {
      id: 41957,
      serviceWindowStart: '2020-09-01T23:59:00.000Z',
      serviceWindowEnd: '2020-09-02T06:33:00.000Z',
      materialNumber: null,
      emplacement: [
        {
          id: 28455,
          emplacementStart: '2020-09-01T23:59:00.000Z',
          emplacementEnd: '2020-09-02T06:33:00.000Z',
          durationInMinutes: 394,
          maintenanceTrack: null,
          inConflict: false,
          serviceWindowId: 41957,
        },
      ],
    },
  },
  {
    id: 41950,
    departureDate: '2020-09-02T10:33:00.000Z',
    arrivalDate: '2020-09-01T19:59:00.000Z',
    materialNumber: '4165',
    trainNumberArrival: '3517',
    trainNumberDeparture: '2587',
    trainPosition: '101',
    trainStation: null,
    trainIOD: true,
    noDepartureTime: false,
    hidden: false,
    locationId: 5341,
    serviceWindowId: 41981,
    location: {
      id: 5341,
      locationNumber: 654000,
      name: 'Utrecht',
      region: 'Regio Randstad Zuid',
      locationType: 'SB',
      code: null,
      shuntingTime: null,
      minOverstandMinutes: null,
      station: 'UT',
      NTTstation: null,
      isWorkLocation: null,
      implemented: false,
      autoplan: false,
      autoplanComodity: false,
      address: null,
      commodityGroups: null,
      distance: null,
      defaultAssignmentPersonId: null,
      defaultReasonMaintenance: 'Service',
      knoopPunt: null,
    },
    serviceWindow: {
      id: 41981,
      serviceWindowStart: '2020-09-01T19:59:00.000Z',
      serviceWindowEnd: '2020-09-02T10:33:00.000Z',
      materialNumber: null,
      emplacement: [
        {
          id: 28479,
          emplacementStart: '2020-09-01T19:59:00.000Z',
          emplacementEnd: '2020-09-02T10:33:00.000Z',
          durationInMinutes: 874,
          maintenanceTrack: null,
          inConflict: false,
          serviceWindowId: 41981,
        },
      ],
    },
  },
];
