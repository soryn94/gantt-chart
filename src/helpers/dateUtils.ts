import addDays from 'date-fns/addDays';
import differenceInMinutes from 'date-fns/differenceInMinutes';
import lightFormat from 'date-fns/lightFormat';
import subDays from 'date-fns/subDays';

export function differenceBetweenDates(date1: number|Date, date2: number|Date): number {
  return differenceInMinutes(date1, date2);
}

export function formatDateTime(date: number|Date): string {
  return lightFormat(date, 'dd/MM HH:mm');
}

export function formatTime(date: number|Date): string {
  return lightFormat(date, 'HH:mm');
}

export function addDaysQuantity(date: number|Date, quantity: number): Date {
  return addDays(date, quantity);
}

export function subtractDaysQuantity(date: number|Date, quantity: number): Date {
  return subDays(date, quantity);
}

export default {
  addDaysQuantity,
  differenceBetweenDates,
  formatDateTime,
  formatTime,
  subtractDaysQuantity,
};
